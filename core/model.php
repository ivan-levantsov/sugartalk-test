<?php

abstract class Model
{
	/**
	 * Source: database
	 * @var Database
	 */
	protected $db;

	/**
	 * Set of model's fields
	 */
	protected $fields = array();
	
	/**
	 * Set of user's fields validation rules
	 * @var array
	 */
	protected $fieldsRules = array();

	/**
	 * Corresponding table in DB if exists
	 * @var string
	 */
	protected $table = '';

	/**
	 * Table's primary key's column
	 * @var string
	 */
	protected $primaryKey = '';

	/**
	 * Generate UUID for primary key
	 * @vat bool
	 */
	protected $useUuid = false;
	
	/**
	 * Set user's data
	 * @return bool
	 */
	abstract function set($data);

	/**
	 * Set default configuration for model
	 */
	public function __construct()
	{
		$this->setSource(Mvc::app()->database());
	}

	/**
	 * Set database source
	 * @param Database $source
	 */
	public function setSource($source)
	{
		$this->db = $source;
	}

	/**
	 * Return database object
	 * @return Object
	 */
	public function getSource()
	{
		return $this->db;
	}

	/**
	 * Validate user data
	 * @param array $data
	 * @return boolean
	 */
	public function validate($data)
	{
		$rulesFunctions = array(
			'required' => function($data, $field, $ruleValue) {
				return HelperCommon::iVal($data, $field, '') != '';
			},
			'max' => function($data, $field, $ruleValue) {
				return strlen(HelperCommon::iVal($data, $field, '')) <= $ruleValue;
			},
		);

		foreach ($this->fieldsRules as $field => $rules) {
			foreach ($rules as $rule => $ruleValue) {
				$result = $rulesFunctions[$rule]($data, $field, $ruleValue);
				if (!$result) {
					return 'Поле ' . $field . ' заполнено неверно.';
				}
			}
		}
		
		return true;
	}

	/**
	 * Insert record into database
	 */
	public function insert()
	{
		$this->db->insert($this->table, $this->fields, $this->primaryKey, $this->useUuid);
	}

	/**
	 * Update record using primary key
	 * @param array $fields
	 */
	public function update($fields)
	{
		$this->db->update($this->table, $fields, $this->primaryKey, $this->fields[$this->primaryKey]);
	}

	/**
	 * Get model's field
	 * @param string $name
	 * @return any
	 */
	public function __get($name)
	{
		return $this->fields[$name];
	}
}