<?php

abstract class Database
{
	/**
	 * Connection object
	 * @var object
	 */
	protected $connection;
	
	protected $host = '';
	protected $port = 0;
	protected $name = '';
	protected $username = '';
	protected $password = '';

	protected $lastError = '';

	/**
	 * Set default connection settings
	 * @param string $host
	 * @param int $port
	 * @param string $username
	 * @param string $password
	 * @param string $name
	 */
	public function __construct($host, $port, $username, $password, $name)
	{
		$this->host = $host;
		$this->port = $port;
		$this->username = $username;
		$this->password = $password;
		$this->name = $name;
	}

	/**
	 * Connect to database, set connection object
	 */
	abstract function connect();

	/**
	 * Execute query to database
	 * @param $queryString string
	 */
	abstract function query($queryString);

	/**
	 * Set database error
	 */
	abstract function setLastError();

	/**
	 * Return last database error
	 * @return string
	 */
	public function getLastError()
	{
		return $this->lastError;
	}
}
