<?php

abstract class Controller
{
	/**
	 * Current name of controller's action
	 * @var $action string
	 */
	public $action = '';
	
	/**
	 * Is ajax flag
	 * @var boolean
	 */
	public $isNotHtml = false;

	/**
	 * List of included javascript code
	 */
	private $_includedJsViews = '';

	/**
	 * It displays by default
	 */
	public function index()
	{
		
	}

	/**
	 * Start bufferization
	 */
	public function __construct()
	{
		ob_start();
	}

	/**
	 * Display all content
	 */
	public function __destruct()
	{
		if (!$this->isNotHtml) {
			$this->_display();
		}
	}

	/**
	 * Load view (twig template) and js view if exists
	 * @param string $view
	 * @param array $params
	 * @param bool $returnHtml
	 * @return string
	 */
	public function render($view, $params = array(), $returnHtml = false)
	{
		$twig = Mvc::app()->twig();
		$twigHtmlPath = '/html/' . $view . '.html';
		$twigJsPath = '/js/' . $view . '.js.html';
		$pathHtml = Mvc::app()->dir() . 'views' . $twigHtmlPath;

		$result = '';
		if(is_file($pathHtml)) {
			$view = $twig->loadTemplate($twigHtmlPath);

			if ($returnHtml) {
				$result = $view->render($params);
			}
			else {
				$view->display($params);
			}
		}

		$pathJs = Mvc::app()->dir() . 'views' . $twigJsPath;
		if(is_file($pathJs)) {
			$view_js = $twig->loadTemplate($twigJsPath);
			$this->_includedJsViews .= $view_js->render($params);
		} else {
			if(!is_file($pathHtml)) {
				throw new Exception('View ' . $pathHtml . ' is undefined');
			}
		}
		return $result;
	}

	/**
	 * Load model by name
	 * @param string $model
	 * @return object
	 */
	public function loadModel($model)
	{
		$modelPath = mvc::app()->dir() . 'models/' . strtolower($model) . '.php';
		if (!is_file($modelPath)) {
			throw new Exception('Model "' . $modelPath . '" is not found');
		}

		$modelClass = 'Model' . ucfirst($model);
		
		require_once $modelPath;
		return new $modelClass;
	}

	/**
	 * Display layout with content
	 */
	private function _display()
	{
		//-------------------------------------------------
		// flush buffer
		//-------------------------------------------------
		$content = ob_get_contents();
		ob_end_clean();

		//-------------------------------------------------
		// render layout
		//-------------------------------------------------
		$this->render('layout', array(
			'content'			=> $content,
			'applicationTitle'	=> mvc::app()->config('APP_TITLE'),

			'includedJsViews'	=> $this->_includedJsViews,
		));
	}

	/**
	 * Return json answer
	 * @param array	$answer
	 */
	public function jsonAnswer($answer)
	{
		echo json_encode($answer);
		exit();
	}

	/**
	 * Return json error
	 * @param string	$errorText
	 */
	public function jsonError($errorText)
	{
		$this->jsonAnswer(array('error' => $errorText));
	}
}