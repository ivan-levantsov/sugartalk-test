<?php

class MySqlDatabase extends Database
{
	/**
	 * Connect to database, set connection object
	 */
	public function connect()
	{
		$this->connection = mysqli_connect($this->host, $this->username, $this->password, $this->name, $this->port);
		if (mysqli_connect_errno()) {
			$this->setLastError(mysqli_connect_error());
			return false;
		}
		return $this->connection;
	}

	/**
	 * Execute query to database
	 * @param $queryString string
	 */
	public function query($queryString)
	{
		if (empty($this->connection)) {
			return false;
		}
		
		$result = mysqli_query($this->connection, $queryString);
		if (!$result) {
			$this->setLastError();
		}
		return $result;
	}

	/**
	 * Execute query to database and return assoc array
	 * @param $queryString string
	 */
	public function queryArray($queryString)
	{
		$array = array();
		$result = $this->query($queryString);
		while ($assocResult = mysqli_fetch_assoc($result)) {
			$array[] = $assocResult;
		}
		return $array;
	}

	/**
	 * Execute insert query
	 * @param string $table
	 * @param array $fields
	 * @param string $primaryKey
	 * @param boolean $useUuid
	 */
	public function insert($table, $fields, $primaryKey = '', $useUuid = false)
	{
		$queryString = "INSERT INTO `" . $table . "` ";

		$columns = array();
		$values = array();
		if ($useUuid) {
			$columns[] = '`' . $primaryKey . '`';
			$values[] = "UUID()";
		}
		foreach ($fields as $field => $value) {
			$columns[] = "`" . $field . "`";
			$values[] = "'" . addslashes($value) . "'";
		}

		$queryString .= "(" . implode(',', $columns) . ")";
		$queryString .= " VALUES (" . implode(',', $values) . ")";

		return $this->query($queryString);
	}

	/**
	 * Execute update query
	 * @param string $table
	 * @param array $fields
	 * @param string $primaryKey
	 * @param string $primaryKeyValue
	 */
	public function update($table, $fields, $primaryKey, $primaryKeyValue)
	{
		$queryString = "UPDATE `" . $table . "` SET ";
		
		$columns = array();
		foreach ($fields as $field => $value) {
			$columns[] = "`" . $field . "` = '" . addslashes($value) . "'";
		}

		$queryString .= implode(', ', $columns);
		$queryString .= " WHERE `" . $primaryKey . "` = '" . addslashes($primaryKeyValue) . "'";

		echo $queryString;
		return $this->query($queryString);
	}

	/**
	 * Set database error
	 */
	public function setLastError()
	{
		$this->lastError = mysqli_error($this->connection);
	}
}