<?php
/**
 * Simple MVC singleton
 */
require_once 'core/controller.php';
require_once 'core/database.php';
require_once 'core/mysql.php';
require_once 'core/model.php';

class Mvc
{
	/**
	 * Root dir
	 * @var string
	 */
	private static $_rootDir = '';

	/**
	 * Instance of mvc
	 * @var Mvc
	 */
	private static $_mvc = null;

	/**
	 * Config array
	 * @var array
	 */
	private $_config = array();
	
	/**
	 * Routes array
	 * @var array
	 */
	private $_routes = array();

	/**
	 * Current controller
	 * @var Controller
	 */
	private $_currentController = null;

	/**
	 * Twig templates
	 * @var Twig_Environment
	 */
	private $_twig = null;

	/**
	 * Database
	 * @var Database
	 */
	private $_db = null;


	//protect construct and clone methods
	private function __construct() {}
	private function __clone() {}
	
	/**
	 * Create MVC singleton
	 *
	 * @param string	$localConfig config array
	 */
	public static function initialization($localConfig = array())
	{
		if (!isset(self::$_mvc)) {
			self::$_mvc = new Mvc();
			session_start();
		}
		
		//set rootDir
		self::$_rootDir = __DIR__ . '/../';

		//load config details, override config
		$config = self::loadArrayFromFile('config/config_details.php');
		if (empty($config)) {
			throw new Exception("Config file is empty'");
		}
		self::$_mvc->_config = array_replace_recursive($config, $localConfig);
		//load routes
		self::$_mvc->_routes = self::loadArrayFromFile('config/routes.php');
		//load twig
		self::$_mvc->_twigInitialization();
		//load database
		self::$_mvc->_loadMySql();

		//-----------------------------------------------
		// Add helpers
		//-----------------------------------------------
		self::$_mvc->loadHelper('common');
		return self::$_mvc;
	}

	/**
	 * Return app Singleton
	 *
	 * @return	fb_batch_request
	 * @throws	Exception
	 */
	public static function app()
	{
		if (is_null(self::$_mvc)) {
			throw new Exception('Application has not been initialized yet');
		}
		return self::$_mvc;
	}


	/**
	 * Return array from file
	 * @param string	$file relative path to file
	 */
	public static function loadArrayFromFile($file)
	{
		return require self::$_rootDir . $file;
	}

	/**
	 * Return value from config
	 * @param	string	$name
	 * @return	mixed
	 */
	public function config($name = '')
	{
		if(!empty($name)) {
			return $this->_config[$name];
		}
		return $this->_config;
	}

	/**
	 * Return twig object
	 * @return Twig_Environment
	 */
	public function twig()
	{
		return $this->_twig;
	}

	/**
	 * Return base application dir
	 * @return string
	 */
	public function dir()
	{
		return self::$_rootDir;
	}

	/**
	 * Return database object
	 * @return Database
	 */
	public function database()
	{
		return $this->_db;
	}

	/**
	 * Load helper
	 * @param string $helperName name of helper
	 */
	public function loadHelper($helperName)
	{
		require_once 'helpers/' . $helperName . '.php';
	}

	/**
	 * Route method
	 * @param string $url
	 */
	public function route($url)
	{
		$url = explode('?', $url);
		$url = array_shift($url);
		foreach ($this->_routes as $route => $routeParams) {
			if ($route == $url) {
				$controller = array_shift($routeParams);
				$method		= array_shift($routeParams);

				if (!empty($method) && HelperCommon::iVal($_SERVER, 'REQUEST_METHOD') != $method) {
					continue;
				}

				return $this->callController($controller);
			}
		}

		//Nothing were found, call 404
		$this->callController('index:show404');
	}

	/**
	 * Call controller
	 * @param string $path
	 * @return bool
	 */
	public function callController($path)
	{
		$method = 'index';
		$path = preg_replace_callback('/:(\w+)$/i', function($matches) use (&$method) {
			$method = $matches[1];
			return '';
		}, $path);
		$path = self::$_rootDir . 'controllers/' . $path . '.php';
		$controllerName = pathinfo($path, PATHINFO_FILENAME);
		if (!is_file($path)) {
			throw new Exception('Controller "' . $controllerName . '" is not defined');
		}
		require_once $path;

		$controllerName .= 'Controller';
		$this->_currentController = new $controllerName();
		$this->_currentController->action = $method;
		$this->_currentController->$method();
		return true;
	}

	/**
	 * Twig initialization
	 */
	private function _twigInitialization()
	{
		require_once self::$_rootDir . 'libraries/Twig/Autoloader.php';
		Twig_Autoloader::register();
		$twig_loader = new Twig_Loader_Filesystem(self::$_rootDir . 'views');
		$this->_twig = new Twig_Environment($twig_loader, array(
			//'debug' 		=> TRUE,
			'cache' 		=> self::$_rootDir . 'views/compilation_cache',
			'auto_reload' 	=> TRUE,
		));
	}

	/**
	 * MySql loading
	 */
	private function _loadMySql()
	{
		$this->_db = new MySqlDatabase(
			self::$_mvc->_config['DATABASE']['HOST'],
			self::$_mvc->_config['DATABASE']['PORT'],
			self::$_mvc->_config['DATABASE']['USERNAME'],
			self::$_mvc->_config['DATABASE']['PASSWORD'],
			self::$_mvc->_config['DATABASE']['NAME']
		);

		$this->_db->connect();
	}
}