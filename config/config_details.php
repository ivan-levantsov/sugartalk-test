<?php
/**
 * Config file.
 * You may override all settings in config.php
 */

return array(
	'PROTOCOL'		=> 'http://',
	'BASE_DOMAIN'	=> 'sugartalk.local',
	'APP_TITLE'		=> 'SugarTalk',

	'DATABASE'		=> array(
		'HOST'			=> 'localhost',
		'PORT'			=> 3306,
		'USERNAME'		=> '',
		'PASSWORD'		=> '',
		'NAME'			=> 'sugartalk',
	),
);