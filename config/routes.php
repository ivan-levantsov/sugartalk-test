<?php
/**
 * Set of all application routes
 */

return array(
	//application 01
	'/'	=> array('form', 'GET'),
	'/captcha' => array('form:captcha', 'GET'),
	'/validate-captcha' => array('form:validateCaptcha', 'POST'),
	'/save'	=> array('form:save', 'POST'),

	//application 02
	'/save-curl' => array('message', 'POST'),
);