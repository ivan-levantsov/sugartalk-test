<?php

//-----------------------------------------------
// Load MVC library files
//-----------------------------------------------
require_once 'core/mvc.php';

//-----------------------------------------------
// Load config files
//-----------------------------------------------
//load local config
$localConfig = mvc::loadArrayFromFile('config/config.php');

//-----------------------------------------------
// Create app singleton
//-----------------------------------------------
$app = mvc::initialization($localConfig);

//-----------------------------------------------
// All cron controllers
//-----------------------------------------------
$app->callController('cron:sendEmails');