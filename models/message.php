<?php

class ModelMessage extends Model
{
	const NAME_SIZE = 255;
	const EMAIL_SIZE = 255;
	const MESSAGE_SIZE = 65535;

	protected $fields = array(
		'name' => '',
		'message' => '',
		'email' => '',
		'sent' => 0,
	);

	protected $table = 'Messages';

	protected $primaryKey = 'id';

	protected $useUuid = true;

	protected $fieldsRules = array(
		'name' => array('required' => true, 'max' => self::NAME_SIZE),
		'email' => array('required' => true, 'max' => self::EMAIL_SIZE),
		'message' => array('required' => true, 'max' => self::MESSAGE_SIZE),
	);

	public $unavailableMailDomains = array(
		'mail.ru',
		'rambler.ru',
		'yandex.ru',
	);

	/**
	 * Set model values
	 * 
	 * @param $data array
	 * @return bool
	 */
	public function set($data)
	{
		$valid = $this->validate($data);
		if ($valid !== true) {
			return $valid;
		}

		$this->fields['name'] = $data['name'];
		$this->fields['message'] = $data['message'];
		$this->fields['email'] = $data['email'];
		$this->fields['sent'] = false;

		return true;
	}

	/**
	 * Get list of messages for last N minutes
	 * @param int $minutes
	 * @return array
	 */
	public function getMessagesForSending($minutes)
	{
		$minutes = intval($minutes);

		$queryString = "
			SELECT id, name, message, email
			FROM `" . $this->table . "`
			WHERE sent = 0 AND date_created >= DATE_SUB(NOW(), INTERVAL " . $minutes . " MINUTE);
		";

		$results = array();
		$resultsArray = $this->db->queryArray($queryString);
		
		foreach($resultsArray as $messageArray) {
			$message = new ModelMessage();
			$message->set($messageArray);
			$message->fields['id'] = $messageArray['id'];
			$results[] = $message;
		}

		return $results;
	}

	/**
	 * Get domain by email
	 * @return string
	 */
	public function getDomain()
	{
		$domain = explode("@", $this->email);
		return array_pop($domain);
	}

	/**
	 * Check that current domain is not in the of unavailable domains
	 * @return boolean
	 */
	public function isAvailable()
	{
		return !empty($this->fields['email']) && !in_array($this->getDomain(), $this->unavailableMailDomains);
	}
}