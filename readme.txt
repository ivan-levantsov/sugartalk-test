This is test task for sugartalk.
Author: Levancov Ivan

To deploy:
1. Do git clone
2. Create config.php, it should extend config_details.php, for example it may look like:
<?php

return array(
	'DATABASE'		=> array(
		'HOST'			=> 'localhost',
		'PORT'			=> 3306,
		'USERNAME'		=> 'root',
		'PASSWORD'		=> '',
		'NAME'			=> 'sugartalk',
	),
);
3. Execute forward.sql for database
4. Setup virtual host to directory /htdocs
5. Make folder 'views/html' writable for apache user

To setup cron:
1. Add cron_jobs.php to your cron table.

Application 01: controller controllers/form
Application 02: controller controllers/messages
Application 03: controller controllers/cron