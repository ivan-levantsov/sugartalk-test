<?php

class HelperCommon
{
	/**
	 * Return element with specified 'key' in 'variable'. If element doesn't exist - return 'default' value
	 * @param mixed $variable
	 * @param array $key array('elem1', 'elem2', 'elem3') - return $array['elem1']['elem2']['elem3']
	 * @param string $default
	 * @return string
	 */
	public static function iVal($variable, $key, $default = '')
	{
		if (is_object($variable)) {
			if (isset($variable->$key)) {
				return $variable->$key;
			} else {
				return $default;
			}
		}
		elseif (is_array($variable)) {
			if (is_array($key)) {
				foreach ($key as $currentKey){
					if (!isset($variable[$currentKey])){
						return $default;
					}
					$variable = $variable[$currentKey];
				}
				return $variable;
			}

			if (isset($variable[$key])) {
				return $variable[$key];
			}
		}

		return $default;
	}
}
