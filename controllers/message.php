<?php

/**
 * Application 02: get data from application 1 and save into database
 */
class MessageController extends Controller
{
	public function index()
	{
		$this->isNotHtml = true;

		$json = file_get_contents('php://input');
		$data = json_decode($json, true);

		if (empty($data)) {
			$this->jsonError('Incorrect data.');
		}

		$message = $this->loadModel('message');
		$result = $message->set($data);
		if ($result !== true) {
			$this->jsonError($result);
		}
		$message->insert();

		if ($message->getSource()->getLastError()) {
			$this->jsonError("Database error, please contact to administrator.");
		}
		$this->jsonAnswer(array('ok' => true));
	}
}