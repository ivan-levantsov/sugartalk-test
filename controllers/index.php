<?php

/**
 * Default application controller
 */
class IndexController extends Controller
{
	/**
	 * Show default 404 page
	 */
	public function show404()
	{
		header('HTTP/1.0 404 Not Found');
		$this->render('errors/404');
	}
}