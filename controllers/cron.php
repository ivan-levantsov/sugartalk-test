<?php

/**
 * Application 03: send email by cron
 */
class CronController extends Controller
{
	public function sendEmails()
	{
		$this->isNotHtml = true;

		$message = $this->loadModel('message');

		//collect emails for last 10 minutes
		$messages = $message->getMessagesForSending(10);

		foreach ($messages as $message) {
			$template = $message->isAvailable() ? 'available' : 'unavailable';
			$subject = $message->isAvailable() ? 'Все отлично' : 'К сожалению, мы не можем вам помочь';
			
			$text = $this->render('emails/' . $template, array(
				'domain' => $message->getDomain(),
				'name' => $message->name,
				'message' => $message->message
			), true);

			mail($message->email, $subject, $text);
			
			$message->update(array(
				'sent' => 1,
			));
		}
	}
}