<?php

/**
 * Application 01: frontend
 */
class FormController extends Controller
{
	/**
	 * Show form
	 */
	public function index()
	{
		$this->loadModel('message');

		$this->render('form', array(
			'nameSize' => ModelMessage::NAME_SIZE,
			'emailSize' => ModelMessage::EMAIL_SIZE,
			'messageSize' => ModelMessage::MESSAGE_SIZE,
			'captchaParam' => urlencode(microtime()),
		));
	}

	/**
	 * Show captcha
	 */
	public function captcha()
	{
		$this->isNotHtml = true;
		$this->loadCaptchaLibrary();
	}

	/**
	 * Validate captcha
	 */
	public function validateCaptcha()
	{
		$this->isNotHtml = true;

		$code = trim(HelperCommon::iVal($_POST, 'captcha', ''));
		
		if (empty($_SESSION['captcha']['code']) || $code != $_SESSION['captcha']['code']) {
			$this->loadCaptchaLibrary();
			
			$this->jsonAnswer(array(
				'url' => $_SESSION['captcha']['image_src'],
			));
		}

		$_SESSION['captcha']['valid'] = true;
		$this->jsonAnswer(array('ok' => true));
	}

	/**
	 * Load captcha library
	 */
	public function loadCaptchaLibrary()
	{
		include mvc::app()->dir() . "libraries/simple-php-captcha/simple-php-captcha.php";
	}

	/**
	 * Saving data using curl
	 */
	public function save()
	{
		$this->isNotHtml = true;

		$json = HelperCommon::iVal($_POST, 'data', '{}');
		$data = json_decode($json, true);

		if (empty($data)) {
			$this->jsonError('Incorrect data.');
		}

		if (empty($_SESSION['captcha']['valid'])) {
			$this->jsonError('Incorrect captcha.');
		}

		$message = $this->loadModel('message');
		$result = $message->set($data);
		if ($result !== true) {
			$this->jsonError($result);
		}

		$curl = curl_init();
		$url = Mvc::app()->config('PROTOCOL') . Mvc::app()->config('BASE_DOMAIN') . '/save-curl';
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
		$out = curl_exec($curl);
		echo $out;
		curl_close($curl);
	}
}