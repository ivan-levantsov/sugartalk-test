/*=== query - levancov - 27.07.2015 ===*/
CREATE TABLE `Messages` (
  `id` CHAR(48) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `message` TEXT NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `date_created` DATETIME NOT NULL DEFAULT NOW(),
  `sent` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`));
